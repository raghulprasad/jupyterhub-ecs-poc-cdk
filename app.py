#!/usr/bin/env python3
import os

from aws_cdk import core as cdk
from cdk.jupyter_ecs_service.jupyter_ecs_service_stack import JupyterEcsServiceStack
from cdk.jupyter_ecs_service.constants import BASE_NAME

# env_USA = cdk.Environment(account="877969058937", region="us-east-1")
app = cdk.App()
JupyterEcsServiceStack(app, BASE_NAME)

app.synth()
